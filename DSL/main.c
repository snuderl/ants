///TODOS:
/// If a explorer ant finds conflicting trail go back a few markers and change direction

turn(Direction dir){

}

bool sweep(Cond cond){
	turn(left)
	if(sense(cond, ahead)) return true
    else if(sense(cond, leftAhead))
    return false
}


findFood(){
	if(sweep(Food)){
		pickup()
	}
	else{
		walk(1)
		sweep(cond)
	}
}

creatingPathHome(){
	turn(180°);

	recur{
		unmark(somenumber);
		moveToNextMarker();
		if(home){
			dropFood();
			searchForTrails();
		}
	}

}


creatingPath(int markerNumber){
	placeMarker(markerNumber)
	if(walk(1)){
		if(findFood){
			pickup()
			createPathHome()
		}
		else{
			creatingPath((markerNumber+1) % 3)
		}
	}
	else{
		changeDirection()
		creatingPath((markerNumber + 1) % 3)
	}
	
}

int FOODTRAIL = 4;

followFoodTrailHome(int index){
	if(home()){
		dropFood();
		followFoodTrail(false);
	}
	else{
		if(sense(leftAhead, next(index))){
			walk(1);
		}else if(sense(Ahead, next(index))){
			turn(Right);
		}else{
			walk(1);
			turnLeft();
		}
		followFoodTrailHome(next(index));
	}
}



///Orientate urself in the right food direction next to the trail
/// If u want to get to food, folow the increasing sequence and stay on right side.
/// Way back is decreasing sequence and on the left side.
orientate(bool hasFood){
	if(senseAhead(5)){
		if(hasFood){
			turnLeft();
			walk(1);
			turnRight();
		}
		else{
			turnRight();
			walk(1);
			turnLeft();	
		}
	}
	else{
		turn(1);
		orientate(hasFood);
	}
}


searchForTrails(){
	if(sensedFoodTrail()){
		followFoodTrail(false);
		stepOnTrail();//assume its ahead
		moveOnBaseMarker(); ///we allways orienate from here
		orienate();
	}
	else{
		rwalk1();
	}
}


int row(){
	if(aheadFree()){
		walk(1)
		return 1
	}
	else{
		return row()+1;
	}
}

int r = row()
if(r == 1){
	createPath();
}else if(r == 2){
	searchForTrails()
}
else{
	die;
}



first5ants creatingPath
next20 searchForTrails
last10 protecthome