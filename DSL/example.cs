class Ants {

  void main() {
    if(flip(5)) {
      attack();
    } else {
      searchFoodRandom();
    }
  }
  
  void attack() {
    while(true) {
      if(sense(Here, FoeHome)) {
        while(true) {
          if(sense(Here, Food)) {
            pickup();
          }
        
          if(sense(Ahead, FoeHome)) {
            move();
          } else {
            turn(Right);
            if(flip(2)) { turn(Right); }
            if(flip(2)) { turn(Right); }
          }
        }
      } else if(sense(Ahead, FoeHome)) {
        enterHome(FoeHome);
      } else {
        stepRandom();
      }
    }
  }
  
  //Will try to enter a home, make sure you are positioned Ahead of it.
  void enterHome(Condition home) {
    while(true) {
      if(sense(Here, home)) {
        return;
      }
      
      if(sense(Ahead, Foe)) {
      } else if(sense(Ahead, Friend)) {
      } else {
        move();
      }
    }
  }
  
  void searchFoodRandom() {
    while(true) {
      if(sense(Here, Food)) {
        if(pickup()) {
          //Turn around
          rotate(180);
          
          searchHomeRandom();
        }
      } else {
        //Walk randomly
        stepRandom();
      }
    }
  }
  
  void searchHomeRandom() {
    while(true) {
      //Check if home
      if(sense(Ahead, Home)) {
        enterHome(Home);
        //move();
      } else if(sense(RightAhead, Home)) {
        turn(Right);
      } else if(sense(LeftAhead, Home)) {
        turn(Left);
      }
      
      if(sense(Here, Home)) {
        //drop and walk back out
        drop();
        rotate(180);
        while(sense(Here, Home)) { move(); }
        
        return;
      } else {
        //Walk randomly
        stepRandom();
      }
    }
  }
  
  void stepRandom() {
    //Walk randomly
    if(flip(10)) {
      if(flip(2)) {
        turn(Right);
      } else {
        turn(Left);
      }
    }
    move();
  }
  
  void rotate(int degrees) {
    if(degrees <= 180) {
      if(degrees -  60 >= 0){ turn(Right); }
      if(degrees - 120 >= 0){ turn(Right); }
      if(degrees - 180 >= 0){ turn(Right); }
    } else {
      if((360 - degrees) -  60 >= 0){ turn(Left); }
      if((360 - degrees) - 120 >= 0){ turn(Left); }
    }
  }
  
  // ## ################## ##
  // ## DEPRECATED METHODS ##
  // ## ################## ##
  
  void searchNewFood() {
    while(true) {
      if(sense(Here, Food)) {
        if(pickup()) {
          //Turn around
          turn(Right);
          turn(Right);
          turn(Right);
	        
	      while(sense(Here, Marker, 5)) {
  	        //first tile
	        unmark(5);
	        mark(3);
	        walkTo(5, true, true);
	      
	        //second tile
	        if(sense(Here, Marker, 5)) {
  	          unmark(5);
	          mark(2);
	          walkTo(5, true, true);
	        }
	      
	        //third tile
	        if(sense(Here, Marker, 5)) {
 	          unmark(5);
	          mark(1);
	          walkTo(5, true, true);
	        }
	      }
	    
          move();
	      if(sense(Here, Home)) {
	        drop();
	      } else {
	        while(true) { turn(Right); }
	      }
        }
      } else {        
        //Check whether to turn or not
        if(flip(20)) {
          if(flip(2)) {
            turn(Right);
          } else {
            turn(Left);
          }
        }
        
        //Walk
        if(move()) {
          mark(5);
        } else {
          //Rotate randomly
          if(flip(2)) {
            turn(Right);
            if(flip(2)) {
              turn(Right);
            }
          } else {
            turn(Left);
            if(flip(2)) {
              turn(Left);
            }
          }
        }
      }
    }
  }

  void main1() {
    if(sense(Ahead, Food)) {
    	if(move()) {
    		flip(1);
    	} else {
    		mark(2);
    	}
    	if(true) {
    	  drop();
    	}
    }
    
    while(false || true) {
      move();
    }
    
    a(true);
    
    /* if(b()) {
      move();
    } */
    
    //last state, will point back to start
    mark(5);
  }
  
  void a(bool test) {
    if(test) {
      pickup();
    } else {
      drop();
    }
  }
  
  bool b() {
    return sense(Ahead, Food);
  }
  
  void walkPath(int m1, int m2, int m3) {
    if(sense(Here, Marker, m1)) {
   	  walkTo(m2, true, true);
    } else if(sense(Here, Marker, m2)) {
      walkTo(m3, true, true);
    } else if(sense(Here, Marker, m2)) {
      walkTo(m1, true, true);
    }
  }
  
  //Walks to the requested mark, will turn if needed
  void walkTo(int m, bool t, bool w) {
    if(sense(Ahead, Marker, m)) {
   	} else if(sense(LeftAhead, Marker, m)) {
   	  turn(Left);
   	} else if(sense(RightAhead, Marker, m)) {
   	  turn(Right);
   	} else {
   	  if(t) {
   	    //turn 180 degrees
   	    turn(Right);
   	    turn(Right);
   	    turn(Right);
   	  
   	    walkTo(m, false, false);
   	  }
   	}
   	
   	//Only move when we are allowed to walk
    if(w) {
      move();
   	}
  }
  
  
}