module CSharpLex where

import Data.Char
import Control.Monad
import ParseLib.Abstract

data Token = POpen    | PClose        -- parentheses     ()
           | SOpen    | SClose        -- square brackets []
           | COpen    | CClose        -- curly braces    {}
           | Comma    | Semicolon
           | KeyIf    | KeyElse  
           | KeyWhile | KeyReturn 
           --- | KeyTry   | KeyCatch
           | KeyClass | KeyVoid
           | StdType   String         -- the standard types
           | Operator  String         -- the operators
           | UpperId   String         -- uppercase identifiers
           | LowerId   String         -- lowercase identifiers
           | ConstInt  Int
           | ConstBool Bool
           --- | ConstChar Char
           | ConstSenseDir String
           | ConstCond     String
           | ConstLR       String
           | TInstr    String
           deriving (Eq, Ord, Show)

keyword :: String -> Parser Char String
keyword []                    = succeed ""
keyword xs@(x:_) | isLetter x = do
                                  ys <- greedy (satisfy isAlphaNum)
                                  guard (xs == ys)
                                  return ys
                 | otherwise  = token xs

greedyChoice :: [Parser s a] -> Parser s a
greedyChoice = foldr (<<|>) empty

terminals :: [(Token, String)]
terminals =
    [( POpen     , "("      )
    ,( PClose    , ")"      )
    ,( SOpen     , "["      )
    ,( SClose    , "]"      )
    ,( COpen     , "{"      )
    ,( CClose    , "}"      )
    ,( Comma     , ","      )
    ,( Semicolon , ";"      )
    ,( KeyIf     , "if"     )
    ,( KeyElse   , "else"   )
    ,( KeyWhile  , "while"  )
    ,( KeyReturn , "return" )
    -- ,( KeyTry    , "try"    )
    -- ,( KeyCatch  , "catch"  )
    ,( KeyClass  , "class"  )
    ,( KeyVoid   , "void"   )
    ]


lexWhiteSpace :: Parser Char String
lexWhiteSpace = greedy (satisfy isSpace)

-- | Parses C#-style comments
lexComments :: Parser Char String
lexComments = -- Regular comments: //
              keyword "//" <* greedy (satisfy (/= '\n'))
            <|>
              -- Block comments /* ... */
              keyword "/*" <* many (satisfy (/= '*') <|> symbol '*' <* satisfy (/= '/')) <* keyword "*/"
              
-- | Parses all spaces followed by comments
lexWhiteSpaceAndComments :: Parser Char String
lexWhiteSpaceAndComments = lexWhiteSpace <* greedy (lexComments <* lexWhiteSpace)
                           

lexLowerId :: Parser Char Token
lexLowerId =  (\x xs -> LowerId (x:xs))
          <$> satisfy isLower
          <*> greedy (satisfy isAlphaNum)

lexUpperId :: Parser Char Token
lexUpperId =  (\x xs -> UpperId (x:xs))
          <$> satisfy isUpper
          <*> greedy (satisfy isAlphaNum)

lexConstInt :: Parser Char Token
lexConstInt = (ConstInt . read) <$> greedy1 (satisfy isDigit)

-- | Parses a constant boolean
lexConstBool :: Parser Char Token
lexConstBool = choice (map (\ (t,s) -> ConstBool t <$ keyword s) values)
             where values = [(True, "true"), (False, "false")]
             
---- | Parses a constant char
--lexConstChar :: Parser Char Token
--lexConstChar = ConstChar <$ symbol '\'' <*> anySymbol <* symbol '\''

lexEnum :: (String -> Token) -> [String] -> Parser Char Token
lexEnum f xs = f <$> choice (map keyword xs)

lexTerminal :: Parser Char Token
lexTerminal = choice (map (\ (t,s) -> t <$ keyword s) terminals)

stdTypes :: [String]
stdTypes = ["int", "bool", "Condition", "SenseDir", "LeftOrRight" {-, "long", "double", "float",
            -- "byte", "short", "bool", "char"-}]

operators :: [String]
operators = [ {-"+=",-} "+", {-"-=",-} "-", {-"*=", "*", 
             "/=", "/", "%=", "%", -} "&&", "||",
             {-"^=", "^",-} "<=", "<", ">=", ">", 
             "==", "!=", "="]

instructions :: [String]
instructions = ["sense", "mark", "unmark", "pickup",
                "drop", "turn", "move", "flip"]
                
senseDirs :: [String]
senseDirs = ["Here", "Ahead", "LeftAhead", "RightAhead"]

conditions :: [String]
conditions = ["FriendWithFood",  "FoeWithFood", "Food", 
              "Rock", "FoeMarker", "Home", 
              "FoeHome", "Friend", "Foe", "Marker"]
              
leftOrRight :: [String]
leftOrRight = ["Left", "Right"]

lexToken :: Parser Char Token
lexToken = greedyChoice
             [ lexTerminal
             , lexEnum StdType stdTypes
             , lexEnum Operator operators
             , lexEnum TInstr instructions
             , lexEnum ConstSenseDir senseDirs
             , lexEnum ConstCond conditions
             , lexEnum ConstLR leftOrRight
             , lexConstInt
             , lexConstBool
             -- , lexConstChar
             , lexLowerId
             , lexUpperId
             ]

lexicalScanner :: Parser Char [Token]
lexicalScanner = lexWhiteSpaceAndComments 
                   *> greedy (lexToken <* lexWhiteSpaceAndComments) <* eof

sInstruction :: Parser Token Token
sInstruction = satisfy isInstruction
       where isInstruction (TInstr _) = True
             isInstruction _          = False
             
sSenseDir :: Parser Token Token
sSenseDir = satisfy isSenseDir
       where isSenseDir (ConstSenseDir _) = True
             isSenseDir _             = False
             
sCondition :: Parser Token Token
sCondition = satisfy isCondition
       where isCondition (ConstCond _) = True
             isCondition _         = False
             
sLR :: Parser Token Token
sLR = satisfy isLR
       where isLR (ConstLR _) = True
             isLR _       = False

sStdType :: Parser Token Token
sStdType = satisfy isStdType
       where isStdType (StdType _) = True
             isStdType _           = False

sUpperId :: Parser Token Token
sUpperId = satisfy isUpperId
       where isUpperId (UpperId _) = True
             isUpperId _           = False

sLowerId :: Parser Token Token
sLowerId = satisfy isLowerId
       where isLowerId (LowerId _) = True
             isLowerId _           = False

sConst :: Parser Token Token
sConst  = satisfy isConst
       where isConst (ConstInt  _) = True
             isConst (ConstBool _) = True
             -- isConst (ConstChar _) = True
             isConst (ConstSenseDir _) = True
             isConst (ConstCond _) = True
             isConst (ConstLR _) = True
             isConst _             = False
             
sInt :: Parser Token Token
sInt  = satisfy isInt
       where isInt (ConstInt  _) = True       
             isInt _             = False
             
sOperator :: String -> Parser Token Token
sOperator s = satisfy isOperator
       where isOperator (Operator o) = s == o
             isOperator _            = False
             

sSemi :: Parser Token Token
sSemi =  symbol Semicolon

