module FSM where

import Data.Map hiding (filter)
import CSharpLex
import CSharpGram


type AntState     = Int
type MarkerNumber = Int -- 0..5
type Comment      = String

data Instruction 
   = Sense SenseDir AntState AntState Condition (Maybe Comment)
   | Mark MarkerNumber AntState                 (Maybe Comment)
   | Unmark MarkerNumber AntState               (Maybe Comment)
   | PickUp AntState AntState                   (Maybe Comment)
   | Drop AntState                              (Maybe Comment)
   | Turn LeftOrRight AntState                  (Maybe Comment)
   | Move AntState AntState                     (Maybe Comment)
   | Flip Int AntState AntState                 (Maybe Comment)
   -- This one is purely to allow constant reasoning,
   -- they should not exists in code after compilation is fully done!
   | CInt Int | CBool Bool | CSD SenseDir | CCond Condition | CLR LeftOrRight | CMarker
   | MethCall String [Instruction] AntState
 deriving (Eq, Show)

data LeftOrRight = Left | Right deriving (Eq, Show)
data SenseDir = Here | Ahead | LeftAhead | RightAhead deriving (Eq, Show)
data Condition = Friend | Foe | FriendWithFood | FoeWithFood | Food | Rock | Marker MarkerNumber | FoeMarker | Home | FoeHome deriving (Eq, Show)


-- | The methods that have been declared
-- type MethEnv = [((Type, Token, [Decl]), AntState -> Code)]

-- | The methods that have been converted to states already.
-- | The list contains: (index_in_methenv, next_state, first_state)
-- | index_in_methenv: The index in MethEnv, that matches the same method
-- | next_state      : The state that follows after this method instance, for returning
-- | firs_state      : The first state of this method instance
-- type MethRefEnv = [(Int, Int)]


type Env = Map Token Instruction

-- | The actual code
type Code = [Instruction]

-- | A fragment of code that requires additional data before it can present its code
-- type CodeFragment = MethEnv -> AntState -> Code
type CodeFragment = Env -> AntState -> Code

-- | Formats comments
formatComment :: Maybe Comment -> String
formatComment Nothing = ""
formatComment (Just cm) = " ; " ++ cm

-- | Formats instructions
formatInstr :: Instruction -> String
formatInstr (Sense a b c d cm) = "Sense " ++ show a ++ " " ++ show b ++ " " ++ show c ++ " " ++ show d ++ formatComment cm
formatInstr (Mark a b      cm) = "Mark " ++ show a ++ " " ++ show b ++ formatComment cm
formatInstr (Unmark a b    cm) = "Unmark " ++ show a ++ " " ++ show b ++ formatComment cm
formatInstr (PickUp a b    cm) = "PickUp " ++ show a ++ " " ++ show b ++ formatComment cm
formatInstr (Drop a        cm) = "Drop " ++ show a ++ formatComment cm
formatInstr (Turn a b      cm) = "Turn " ++ show a ++ " " ++ show b ++ formatComment cm
formatInstr (Move a b      cm) = "Move " ++ show a ++ " " ++ show b ++ formatComment cm
formatInstr (Flip a b c    cm) = "Flip " ++ show a ++ " " ++ show b ++ " " ++ show c ++ formatComment cm
formatInstr _                  = error "Unexpected intermediate instruction"

-- | Formats code
formatCode :: Code -> String
formatCode = filter clean . concatMap ((++"\n") . formatInstr)
  where
    clean :: Char -> Bool
    clean _ = True -- notElem x "()\""
    
-- | Generates LeftOrRight from a string
cLeftRight :: String -> LeftOrRight
cLeftRight ("Left")  = FSM.Left
cLeftRight ("Right") = FSM.Right
cLeftRight _         = error "Invalid left or right token"                      
         
-- | Generates SenseDir from a string
cSenseDir :: String -> SenseDir
cSenseDir ("Here")       = Here
cSenseDir ("Ahead")      = Ahead
cSenseDir ("LeftAhead")  = LeftAhead
cSenseDir ("RightAhead") = RightAhead
cSenseDir _              = error "Invalid sense direction token"

-- | Generates Condition from a string
cCond :: String -> Condition
cCond ("Friend")         = Friend
cCond ("Foe")            = Foe
cCond ("FriendWithFood") = FriendWithFood
cCond ("FoeWithFood")    = FoeWithFood
cCond ("Food")           = Food
cCond ("Rock")           = Rock
cCond ("FoeMarker")      = FoeMarker
cCond ("Home")           = Home
cCond ("FoeHome")        = FoeHome
cCond _                  = error "Invalid condition token"
