{-# LANGUAGE RankNTypes #-}
module CSharpCodeFSM where

import CSharpLex
import CSharpAlgebra
import CSharpGram
import FSM
import Data.List (find)
import qualified Data.Map as M

-- Global information
-- The state number that is passed around is the number of the next state!
-- TODO:
-- -- Global variables
-- -- Local variables
-- -- GOTO filtering, should reduce state count


-- | Combines a list of code fragments to a single code fragment.
run :: [CodeFragment] -> CodeFragment
run (t:ts) me ns = let x  = t me ns
                       xs = run ts me (ns + length x)
                   in (x++xs)
run []     _  _  = []

-- | Simple goto instruction, could be filtered afterwards by updating the previous statement
goto :: AntState -> Comment -> Instruction
goto s cm = Flip 2 s s (Just cm)

-- | Creates the actual if-then-else block for instruction calls
runIfI :: (AntState -> AntState -> Maybe Comment -> Instruction) -> CodeFragment -> CodeFragment -> CodeFragment
runIfI e s1 s2 me ns = let x = s1 me (ns + 1)     -- body
                           o = if null x then 0 else 1 + length x
                           y = s2 me (ns + 1 + o) -- else
                       in merge e ns x y
  where
    -- if(...){...}
    merge e' ns' s1' []  = e' ns' (ns' + length s1') (Just "if-then") : s1'
    -- if(...){ } else {...}
    merge e' ns' [] s2'  = e' (ns' + length s2') ns' (Just "if-else") : s2'
    -- if(...){...} else {...}
    merge e' ns' s1' s2' = let x = ns' + length s1' + 1 + length s2'
                               y = e' ns' (ns' + length s1' + 1) (Just "if-then-else")
                           in y : s1' ++ [goto x "skip else block"] ++ s2'
                        
-- | Creates the actual if-block for constants
runIfC :: Bool -> CodeFragment -> CodeFragment -> CodeFragment
runIfC True  s1 _ = s1
runIfC False _ s2 = s2
                        
-- | Creates the actual while block for instruction calls
runWhileI :: (AntState -> AntState -> Maybe Comment -> Instruction) -> CodeFragment -> CodeFragment
runWhileI e s me ns = let x = s me (ns + 1)
                          x' = if null x then x else x ++ [goto (ns-1) "go back to while statement"]
                      in e ns (ns + length x') (Just "while instruction") : x'
                  
-- | Creates the actual while block for constants
runWhileC :: Bool -> CodeFragment -> CodeFragment
runWhileC True  s me ns  = s me ns ++ [goto (ns - 1) "while(true)"]
runWhileC False _ _  _   = [] 
                  
-- | Evaluates an operator
evalOp :: String -> CodeFragment -> CodeFragment -> CodeFragment
evalOp "+"  = evalInt (+)
evalOp "-"  = evalInt (-)
evalOp "&&" = evalBool (&&)
evalOp "||" = evalBool (||)
evalOp "<=" = evalOrd (<=)
evalOp "<"  = evalOrd (<)
evalOp ">=" = evalOrd (>=)
evalOp ">"  = evalOrd (>)
evalOp "==" = evalEq (==)
evalOp "!=" = evalEq (/=)
evalOp _    = error "unknown operator"

-- | Helper function for boolean operator evaluation
evalBool :: (Bool -> Bool -> Bool) -> CodeFragment -> CodeFragment -> CodeFragment
evalBool f e1 e2 env _ = case (e1 env 0, e2 env 0) of
                           ([CBool b1], [CBool  b2]) -> [CBool $ f b1 b2]
                           _                                       -> error "invalid op parameters"

-- | Helper function for integer operator evaluation
evalInt :: (Int -> Int -> Int) -> CodeFragment -> CodeFragment -> CodeFragment
evalInt  f e1 e2 env _ = case (e1 env 0, e2 env 0) of
                           ([CInt i1], [CInt  i2]) -> [CInt $ f i1 i2]
                           _                                     -> error "invalid op parameters"

-- | Helper function for equality operator evaluation
evalEq :: (forall b . Eq b => (b -> b -> Bool)) -> CodeFragment -> CodeFragment -> CodeFragment                           
evalEq   f e1 e2 env _ = case (e1 env 0, e2 env 0) of
                          ([CBool b1], [CBool b2]) -> [CBool $ f b1 b2]
                          ([CInt  i1], [CInt  i2]) -> [CBool $ f i1 i2]
                          ([CSD   d1], [CSD   d2]) -> [CBool $ f d1 d2]
                          ([CCond c1], [CCond c2]) -> [CBool $ f c1 c2]
                          ([CLR   d1], [CLR   d2]) -> [CBool $ f d1 d2]
                          _                        -> error "invalid op parameters"
                          
-- | Helper function for ord comparing operator evaluation
evalOrd :: (forall b . Ord b => (b -> b -> Bool)) -> CodeFragment -> CodeFragment -> CodeFragment                           
evalOrd   f e1 e2 env _ = case (e1 env 0, e2 env 0) of
                          ([CInt  i1], [CInt  i2]) -> [CBool $ f i1 i2]
                          _                        -> error "invalid op parameters"

-- | Executes the parameters and retrieves the method for it
createMethInsn :: Token -> [CodeFragment] -> CodeFragment
createMethInsn (LowerId n) p env ns = [MethCall n (run p env 0) ns]
createMethInsn _           _ _   _  = error "invalid token" 


-- | Determines if the method is the default main method.
isDefaultMethod :: ((Type, String, [Decl]), Env -> AntState -> Code) -> Bool
isDefaultMethod ((TypeVoid, "main", []), _) = True
isDefaultMethod _                                   = False

-- | Find method definition
findMethodD :: String -> [Instruction] -> [((Type, String, [Decl]), CodeFragment)] -> Maybe ((Type, String, [Decl]), CodeFragment)
findMethodD n p (m@((_, n', p'), _):ms) | n == n' && checkType p p' = Just m
                                        | otherwise          = findMethodD n p ms
findMethodD _ _ _ = Nothing
  
-- | Checks if instructions match decaration
checkType :: [Instruction] -> [Decl] -> Bool
checkType [] [] = True
checkType (CBool _:ts) (Decl (TypePrim (StdType "bool"     )) _:ts') = checkType ts ts'
checkType (CInt  _:ts) (Decl (TypePrim (StdType "int"      )) _:ts') = checkType ts ts'
checkType (CCond _:ts) (Decl (TypePrim (StdType "Condition")) _:ts') = checkType ts ts'
checkType (CSD   _:ts) (Decl (TypePrim (StdType "SenseDir" )) _:ts') = checkType ts ts'
checkType (CLR   _:ts) (Decl (TypePrim (StdType "LeftOrRight")) _:ts') = checkType ts ts'
checkType _ _ = False

-- | Find method reference
findMethodR :: String -> [Instruction] -> AntState -> [(String, [Instruction], AntState, AntState)] -> Maybe AntState
findMethodR n p ns ((n', p', ns', fs):ms) | n == n' && p == p' && ns == ns' = Just fs
                                          | otherwise                       = findMethodR n p ns ms
findMethodR _ _ _  _                                                        = Nothing

-- | Create environment
createEnv :: [Instruction] -> [Decl] -> Env
createEnv [] [] = M.empty
createEnv (t:ts) (Decl _ n:ts') = M.insert n t (createEnv ts ts')
createEnv _  _  = error "invalid token"

-- | Fixes any state that points to a state after the last one;
-- | or a state pointing to -1 (return instructions).
-- | Params:: base_state -> next_state -> code
fixOoBState :: AntState -> AntState -> Code -> Code
fixOoBState bs ns c = f (bs + length c) c
  where
    f s (Sense sd ss sf cd cm:xs) = g s ss sf (\x y -> Sense sd x y cd cm) : f s xs
    f s (Mark mn ss        cm:xs) = g s ss 0  (\x _ -> Mark mn x       cm) : f s xs
    f s (Unmark mn ss      cm:xs) = g s ss 0  (\x _ -> Unmark mn x     cm) : f s xs
    f s (PickUp ss sf      cm:xs) = g s ss sf (\x y -> PickUp x y      cm) : f s xs
    f s (Drop ss           cm:xs) = g s ss 0  (\x _ -> Drop x          cm) : f s xs
    f s (Turn lr ss        cm:xs) = g s ss 0  (\x _ -> Turn lr x       cm) : f s xs
    f s (Move ss sf        cm:xs) = g s ss sf (\x y -> Move x y        cm) : f s xs
    f s (Flip i ss sf      cm:xs) = g s ss sf (\x y -> Flip i x y      cm) : f s xs
    f s (MethCall n p ss     :xs) = g s ss 0  (\x _ -> MethCall n p x    ) : f s xs
    f _ []                        = []
    f _ _                         = error "Unexpected intermediate instruction"
                                   
    -- loop to ns if out of bounds                              
    g s s1 s2 co | (s1 == s || s1 == -1) && (s2 == s || s2 == -1) = co ns ns
                 | (s1 == s || s1 == -1)                          = co ns s2
                 |                          (s2 == s || s2 == -1) = co s1 ns
                 | otherwise                                      = co s1 s2
  
-- | Finalize method, by following method calls and appending code to it
finalizeMethod :: Code -> AntState -> [((Type, String, [Decl]), CodeFragment)] -> [(String, [Instruction], AntState, AntState)] -> ([(String, [Instruction], AntState, AntState)], Code)
finalizeMethod [] _ _  mr = (mr, [])
finalizeMethod c  s md mr = let (mr', mc', c') = fix c (s + length c) md mr    -- fix code
                                mc''           = concat mc'                    -- concat new methods
                                s'             = s + length c'                 -- calculate offset
                                (mr'', c'')    = finalizeMethod mc'' s' md mr' -- fix new methods
                            in (mr'', c' ++ c'')
  where
    fix [] s md mr = (mr, [], [])
    fix (MethCall n p ns:cs) s md mr = case findMethodR n p ns mr of
                                      Just fs -> cfix (goto fs $ "invoke: " ++ n) cs [] s md mr
                                      Nothing -> case findMethodD n p md of
                                        Just ((_,_,p'), m) -> let c   = m (createEnv p p') (s+1)
                                                                  re  = (n, p, ns, s)
                                                                  s'  = s + length c
                                                                  c'  = fixOoBState s ns c
                                                              in cfix (goto s $ "invoke: " ++ n) cs [c'] s' md (re:mr) 
                                        Nothing -> error $ "unknown method: " ++ n 
    fix (c:cs) s md mr = cfix c cs [] s md mr
    cfix i cs mc s md mr = let (mr', mc', cs') = fix cs s md mr
                        in (mr', mc ++ mc', i:cs')



-- | The algebra converting CSharp code to Ant instructions.
codeAlgebra :: CSharpAlgebra Code 
                             (((Type, String, [Decl]), Env -> AntState -> Code)) 
                             (CodeFragment) 
                             (CodeFragment)

codeAlgebra = ( (fClas)
              , (fMembDecl,fMembMeth)
              , (fStatDecl,fStatExpr,fStatIf,fStatWhile,fStatReturn,fStatBlock)
              , (fExprCon,fExprVar,fExprOp, fExprMeth, fExprInstr) 
              )
 where
 -- | Handle class declaration
 fClas       _ ms  = case find isDefaultMethod ms of
                       Just (_, m) -> let c = fixOoBState 0 0 (m M.empty 1)
                                          (_, c') = finalizeMethod c 0 ms []
                                      in c'
                       Nothing     -> error "could not find main method"

 -- | Global variables; not supported for now
 fMembDecl   d        = error "not supported"
 
 -- | Method declarations
 fMembMeth   t (LowerId m) ps s = ((t, m, ps), s) -- return method name plus code
 fMembMeth   _ _           _  _ = error " invalid token" 
 
 -- | Local variables; not supported for now
 fStatDecl   d        s me = error "not supported"
 
 -- | Handles simple expressions
 fStatExpr e = e
 
 -- | Handle if-then-else blocks
 fStatIf   e s1 s2 me ns = let e' = e me ns
                           in case e' of
                             [Sense sd _ _ co _] -> runIfI (\x y -> Sense sd x y co) s1 s2 me ns
                             [PickUp{}]          -> runIfI PickUp s1 s2 me ns
                             [Move{}]            -> runIfI Move s1 s2 me ns
                             [Flip i _ _ _]      -> runIfI (Flip i) s1 s2 me ns
                             [CBool b]           -> runIfC b s1 s2 me ns
                             [_]                 -> error "Instruction does not return a boolean"
                             _ -> error "not supported" --TODO: Method invocation, check for boolean type
 
 -- | Handle while blocks
 fStatWhile  e s1  me ns = let e' = e me ns
                           in case e' of
                                [Sense sd _ _ co _] -> runWhileI (\x y -> Sense sd x y co) s1 me ns
                                [PickUp{}]          -> runWhileI PickUp s1 me ns
                                [Move{}]            -> runWhileI Move s1 me ns
                                [Flip i _ _ _]      -> runWhileI (Flip i) s1 me ns
                                [CBool b]           -> runWhileC b s1 me ns
                                [_]                 -> error "Instruction does not return a boolean"
                                _                   -> error "not supported" --TODO: Method invocation, check for boolean type
  
 -- | Handle return instructions
 fStatReturn Nothing     me ns = [goto (-1) "return;"]
 fStatReturn (Just e)    me ns = error "not supported"
 
 -- | Handles code blocks
 fStatBlock = run
 
 -- | Handles constants as intermediate code
 fExprCon (ConstInt  i) _ _ = [CInt i]
 fExprCon (ConstBool b) _ _ = [CBool b]
 fExprCon (ConstSenseDir d) _ _ = [CSD $ cSenseDir d]
 fExprCon (ConstCond     "Marker") _ _ = [CMarker]
 fExprCon (ConstCond     c) _ _ = [CCond $ cCond c]
 fExprCon (ConstLR       d) _ _ = [CLR $ cLeftRight d]
 fExprCon  _            _ _ = error "unknown constant"
 
 -- | Handle variable lookups
 fExprVar v env _ = [env M.! v]
 
 -- | Handles operators; only allowed for reasoning with constants
 fExprOp (Operator o) = evalOp o
 fExprOp _            = error "unexpected token"
 
 -- | Handles method calls
 fExprMeth (LowerId n) ps env ns = [MethCall n (run ps env 0) ns]
 fExprMeth _           _  _   _  = error "invalid token"
 
 -- | Handle sense instructions
 fExprInstr (TInstr "sense") ps env ns = 
   case run ps env ns of
     [CSD sd, CMarker, CInt i] -> if i >= 0 && i <= 5
                                  then [Sense sd ns ns (Marker i) Nothing]
                                  else error " invalid marker number"  
     [CSD sd, CCond c] -> [Sense sd ns ns c Nothing]
     _                 -> error "sense has incorrect parameters"
 
 -- | Handle turn instructions
 fExprInstr (TInstr "turn") ps env ns =
   case run ps env ns of
     [CLR lr] -> [Turn lr ns Nothing]
     _        -> error "turn has incorrect parameters"
 
 -- | Handle remaining instructions
 fExprInstr (TInstr i) ps env ns =
   case run ps env ns of {
     -- | Handle mark, unmark and flip instructions
     [CInt n] -> case i of {
                   "mark"   -> if n >= 0 && n <= 5
                               then [Mark n ns Nothing]
                               else error "invalid marker number";
                   "unmark" -> if n >= 0 && n <= 5
                               then [Unmark n ns Nothing]
                               else error "invalid marker number";
                   "flip"   -> if n >= 1
                               then [Flip n ns ns Nothing]
                               else error "invalid random number";
                   _        -> error $ i ++ " does not accept an int parameter"
                 };
    -- | Handle pickup, drop and move instructions
    []        -> case i of {
                   "pickup" -> [PickUp ns ns Nothing];
                   "drop"   -> [Drop ns Nothing];
                   "move" -> [Move ns ns Nothing];
                   _ -> error "not supported"
                 };
    -- | Unexpected parameters
    _         -> error "invalid parameters"
  }
  
 -- | Something went wrong
 fExprInstr  _          _  _ _ = error "wrong token"