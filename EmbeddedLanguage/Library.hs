{-# LANGUAGE FlexibleInstances, InstanceSigs, DefaultSignatures #-}

module Library where
import Data.Map.Strict
import Control.Monad.State hiding (lift)
import Prelude hiding (repeat, cycle, Left, Right)

data TurnDirection = Left | Right
	deriving Show


data Command = Turn TurnDirection Int
	| Move Int Label
	| Cond Condition SenseDirection Int Int
	| Stay Int
	| Flip Int Label Label
	| Skip Int
	| Drop Int
	| PickUp Label Label
	| Mark Int Label
	| Unmark Int Label
	deriving Show

data Condition = Food | Home | Marker Int | Friend | Rock
	deriving Show

data SenseDirection = Ahead | LeftAhead | RightAhead | Here deriving Show

--- This definitions just wrap types
type Label = Int
type States = Map Label Command
type StatePair = (Label, States)
type Data = State StatePair Label

startingState = (0, empty)

--- Runing the state machine
run :: Data -> (Label, StatePair)
run x = runState (withSelf(\self -> do
	x)) startingState


---------------------------
--Some helpful functions

-- This method provides self label to the function
withSelf :: (Label -> Data) -> Data
withSelf f = do
	counter <- getCounter
	updateCounter (counter + 1)
	updateStore counter (Skip counter)
	return counter

cycle :: Lifter f => (Label -> f) -> Data
cycle f = withSelf(\self ->
	(lifter f) self)

repeat :: Lifter f => Int -> (Label -> f) -> Label -> Data
repeat 1 f = lifter f
repeat n f = do
	f .*. repeat (n-1) f

recurse :: (Lifter f1, Lifter f2) => f1 -> f2 -> Data
recurse from to = do
	l1 <- lift from
	l2 <- lift to
	updateStore l1 (Skip l2)


----------------------------------------------
--- Combinators for chaining commands
(.*.)
  :: (Lifter f1, Lifter f, Lifter f2) =>
     (Label -> f1) -> (Label -> f) -> f2 -> Data
before .*. after = \label -> do
	l <- lift label
	(lifter after) l >>= lifter before

(.*)
  :: (Lifter f1, Lifter f2) =>
     (Label -> f1) -> f2 -> Data
before .* after = do
	l1 <- lift after 
	lifter before l1


-----------------------------------------------------
--- Conditional operators
--- We have 2 versions depending upon whether the last state recievies next
--- state as a paremeter, eg. is of type 'Label -> Data' or is just of type 'Data'
(.??)
  :: (Lifter f1, Lifter f2, Lifter f3) =>
     (Label -> Label -> f1)
     -> f2 -> f3 -> Data
cond .?? left  = \right -> do
	r <- (lift right)
	l <- (lift left)
	(lifter2 cond) l r

cond1 .?! right = cond1 right

(.??.)
  :: (Lifter f2, Lifter f1, Lifter f) =>
     (Label -> Label -> f2)
     -> (Label -> f1) -> (Label -> f) -> Label -> Data
cond .??. left  = \right label -> do
	r <- (lifter right) label
	l <- (lifter left) label
	(lifter2 cond) l r


cond1 .?!. right = cond1 right




--- Anding and oring of 2 state machines
--- We always consider the first parameter to a function
--- to be the succesful case and the second to be the failure.
cond1 `antOr` cond2 = \success failure -> do
	s <- (lift success)
	f <- (lift failure)
	(lifter2 cond1) s .* (lifter2 cond2) s f

cond1 `antAnd` cond2 = \success failure -> do
	s <- lift success
	f <- lift failure
	cond1 
		.?? (cond2 s f)
		.?! f


------
-- This typeclass is used so we can intermingle Commands, Labels 
-- or whole blocks of states inside Data
-- We have 3 methods, depending on the number of parameters

class Lifter f where
	lift :: f -> Data
	lifter :: Lifter f1 => (Label -> f) -> (f1 -> Data)	
	lifter2 :: (Lifter f1, Lifter f2) => (Label -> Label -> f) -> (f1 -> f2 -> Data)

	default lifter :: Lifter f1 => (Label -> f) -> (f1 -> Data)
	lifter f = \x -> do
		l <- lift x
		lift $ f l

	default lifter2 :: (Lifter f1, Lifter f2) => (Label -> Label -> f) -> (f1 -> f2 -> Data)
	lifter2 f = \x y -> do
		l1 <- lift x
		l2 <- lift y
		lift $ f l1 l2

instance Lifter Command where
	lift c = 	
		do 
			(counter, store) <- get
			put (counter + 1, insert counter c store)
			return counter

instance Lifter Label where
	lift = return

instance Lifter (Data) where
	lift = id

--- Optimizer
--- Post processing step to remove Skip states.
clear :: States -> States
clear store = removeSkips (drop 1 $ assocs store) store
removeSkips :: [(Label, Command)] -> States -> States
removeSkips ((from, Skip to) : xs) store = removeSkips xs $ Data.Map.Strict.map (updateCommands (c from to)) store
	where c f t key
		| key == f = t
		| otherwise = key
removeSkips (_:xs) store = removeSkips xs store
removeSkips [] store = Data.Map.Strict.filterWithKey (\k v -> not $ (isSkip v) || (k == 0)) store
	where 
		isSkip (Skip _) = True
		isSkip _ = False

updateCommands changer (Turn dir label) = Turn dir (changer label)
updateCommands changer (Move l1 l2) = Move (changer l1) (changer l2)
updateCommands changer (Cond cond sd l1 l2) = Cond cond sd (changer l1) (changer l2)
updateCommands changer (Flip i l1 l2) = Flip i (changer l1) (changer l2)
updateCommands changer (Drop l1) = Drop (changer l1)
updateCommands changer (PickUp l1 l2) = PickUp (changer l1) (changer l2)
updateCommands changer (Mark i l2) = Mark i (changer l2)
updateCommands changer (Unmark i l2) = Unmark i (changer l2)
updateCommands _ x = x



--- Some low helper functions
getCounter :: Data
getCounter = do
	(counter, _) <- get
	return counter

updateCounter :: Label -> Data
updateCounter c = do
	(_, store) <- get
	put(c, store)
	return c

updateStore :: Label -> Command -> Data
updateStore k v = do
	(c, store) <- get
	put(c, insert k v store)
	return k




--- Text generation
-- Bellow code generates the the .ants text.
generateText :: States -> [String]
generateText store = Prelude.map (\(s, c) -> commandToStr c ++ stateInfo s)  $ assocs store

stateInfo label = "   ; state " ++ show label
comment comments label = case Data.Map.Strict.lookup label comments of
	Just x -> "  ;;  " ++ show x
	Nothing -> ""

commandToStr (Turn dir label) = "Turn " ++ show dir ++ " " ++ show label
commandToStr (Move l1 l2) = "Move " ++ show l1 ++ " " ++ show l2
commandToStr (Flip num l1 l2) = "Flip " ++ show num ++ " " ++ show l1 ++ " " ++ show l2
commandToStr (Skip l1) = "Flip 1 "++ show l1 ++ " " ++ show l1
commandToStr (Drop l1) = "Drop "++ show l1
commandToStr (PickUp l1 l2) = "PickUp " ++ show l1 ++ " " ++ show l2
commandToStr (Cond cond sense l1 l2) = "Sense " ++ show sense ++ " " ++ show l1 ++ " " ++ show l2 ++ " " ++ show cond
commandToStr (Stay l1) = "Turn " ++ show Library.Left ++ " " ++ show l1
commandToStr (Mark i l1) = "Mark " ++ show i ++ " " ++ show l1
commandToStr (Unmark i l1) = "Unmark " ++ show i ++ " " ++ show l1