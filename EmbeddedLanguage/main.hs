import Prelude hiding (Left, Right, repeat, cycle)
import Library


----Explorer ant
explore :: Data
explore = withSelf(\self -> do
	onFood1 <- flip PickUp self .*. findMarker 1 .* followTrail 1 3 2 self self
	onFood2 <- flip PickUp self .*. findMarker 2 .* followTrail 2 1 3 self self
	onFood3 <- flip PickUp self .*. findMarker 3 .* followTrail 3 2 1 self self
	s <- stay
	e1 <- eraseRoad 3 2 1 self
	e2 <- eraseRoad 1 3 2 self
	e3 <- eraseRoad 2 1 3 self

	erase1 <- turn180 .*. sweep (Marker 3) e1 .* s
	erase2 <- turn180 .*. sweep (Marker 1) e2 .* s
	erase3 <- turn180 .*. sweep (Marker 2) e3 .* s

	sweepForFood onFood3
		.*. randomWalk1
		.*. checkAdjacent [1,2,3] erase1 --We have to backtrack
		.*. checkNotMarked [1,2,3] Here erase1
		.*. Mark 1
		.*. sweepForFood onFood1
		.*. randomWalk1
		.*. checkAdjacent [1,2,3] erase2 --We have to backtrack
		.*. checkNotMarked [1,2,3] Here erase2
		.*. Mark 2
		.*. sweepForFood onFood2
		.*. randomWalk1
		.*. checkAdjacent [1,2,3] erase3 --We have to backtrack
		.*. checkNotMarked [1,2,3] Here erase3
		.* Mark 3 self)

eraseRoad m1 m2 m3 next = withSelf(\self ->do
	(flip $ lifter2 (sweep (Marker m1))) next
		.*. walkForce
		.*. Unmark m1
		.*. (flip $ lifter2 (sweep (Marker m2))) next
		.*. walkForce
		.*. Unmark m2
		.*. (flip $ lifter2 (sweep (Marker m3))) next
		.*. walkForce
		.*. Unmark m3
		.* Skip self  
	)

turn180 = repeat 3 (Turn Left)

checkAdjacent xs onFailure onSuccess = do
	onFailure1 <- Turn Right .* onFailure
	onFailure2 <- Turn Left .* onFailure
	checkAdjacentNotMarked xs [LeftAhead, Ahead, RightAhead] onFailure
		.*. Turn Left 
		.*. checkAdjacentNotMarked xs [LeftAhead] onFailure1 
		.*. Turn Right 
		.*. Turn Right
		.*. checkAdjacentNotMarked xs [RightAhead] onFailure2
		.*. Turn Left
		.* onSuccess

checkAdjacentNotMarked xs [] onFailure onSuccess = return onSuccess
checkAdjacentNotMarked xs (d:ds) onFailure onSuccess = 
	checkNotMarked xs d 
	.?? onFailure
	.?! checkAdjacentNotMarked xs ds onFailure onSuccess

checkNotMarked [] dir onFailure onSuccess = return onSuccess
checkNotMarked (x:xs) dir onFailure onSuccess = 
	Cond (Marker x) dir 
	.?? onFailure
	.?! checkNotMarked xs dir onFailure onSuccess

findMarker n next =
	Cond m Ahead next
	.*. Turn Left
	.*. Cond m Ahead next
	.*. Turn Left
	.*. Cond m Ahead next
	.*. Turn Left
	.*. Cond m Ahead next
	.*. Turn Left
	.*. Cond m Ahead next
	.*. Turn Left
	.*. Cond m Ahead next
	.* stay
	where m = Marker n 

followTrail :: Int -> Int -> Int -> Label -> Label -> Data
followTrail s1 s2 s3 onFailure next = withSelf(\self -> do
	cond (Marker s1) stay
	.*. walkForce 
	.*. cond (Marker s2) stay
	.*. walkForce
	.*. cond (Marker s3) stay
	.*. walkForce
	.* self)
	where cond m = do 
		lifter2 $ flip (sweep m)


nestCondition
  :: (Lifter f1, Lifter f2, Lifter f3) =>
     [Label -> Label -> f1]
     -> f2
     -> f3
     -> Data
nestCondition [] onSuccess onFailure = lift onSuccess
nestCondition (x:xs) onSuccess onFailure = do
	(lifter2 (flip x)) (lift onFailure) .* nestCondition xs onSuccess onFailure 

-- Changes direction by n units
turnN 0 next = do
		return next
turnN n next = do 
		cont <- turnN (n - 1) next
		lift $ Turn direction cont
	where direction = if n > 0 then Left else Right

-- Changes directions and continues with next state
turn direction next = do
	lift $ Turn direction next


randomDirection nextState = do
	((Flip 3) 
		.??. const (Turn Left nextState)
		.?!. (Flip 2 nextState) .*. (Turn Right)) nextState

findHome onHome = withSelf (\self -> do
	Cond Home Here onHome 
		.*. walk 1 self 
		.* randomDirection self)


sweepForFood onSuccess onFail = do
	ahead <- walk 1 onSuccess onFail
	let turn dir = lift $ Turn dir ahead
	nestCondition (nested Ahead) ahead
		.*. nestCondition (nested LeftAhead) (turn Left) 
		.* nestCondition (nested RightAhead) (turn Right) onFail
	where nested dir = [Cond Food dir, flip (Cond Home dir)]

-- Look at the tile ahead, left and to the right.
-- If conditition is statisfied, ant is left facing that direction
sweep cond onSuccess onFail = do
	--ahead <- walk 1 onSuccess onFail
	left <- lift $ Turn Left onSuccess
	right <- lift $ Turn Right onSuccess
	Cond cond Ahead onSuccess
		.*. Cond cond LeftAhead left
		.* Cond cond RightAhead right onFail



findFood onFood = withSelf (\self -> do
	isOnFood <- sweepForFood onFood .* changeDirectionProb 7 self
	walk 1 isOnFood 
		.* randomWalk1 self)

changeDirectionProb n next = 
	flip (Flip n) next .* randomDirection next

searchForFood :: Data
searchForFood = withSelf (\self -> do
	goHome <- findHome .* Drop self
	findFood .* PickUp goHome self)


-- Walks a given number of units into given direction. After go to state x. If fail go to state f.
--walk :: Int -> Label -> Label -> Data
walk :: (Lifter f1, Lifter f2) => Int -> f1 -> f2 -> Data
walk 0 x f = lift x
walk n x f = do
	success <- lift x
	failure <- lift f
	(flip Move) failure .* walk (n - 1) success failure


-- Walk in a given direction until failure. Then continue with on failState.
walkUntilFail onFail = withSelf (\self -> do
	walk 1 self onFail)

-- Ant will move until it hits an obstacle, then change direction
randomWalkUntilFail = withSelf (\self -> do
	randomDirection 
		.*  walkUntilFail self)

-- Ant will move one step and then randomly change direction
randomWalk cond onSuccess = withSelf (\self -> do
	randomDirection 
		.* walk 5 self self)

-- stays in place
stay = withSelf return

-- stop everything after finding food
findFoodAndStay = do
	findFood stay

createPathHome :: Label -> Data
createPathHome nextState = stay

randomWalk1WithFail next onFail = Flip 9 
	.?? (randomDirection .* walk 1 next onFail)
	.?! walk 1 next onFail

randomWalk1 :: Label -> Data
randomWalk1 next = withSelf(\self ->
	Flip 9 
	.?? (randomDirection .* walk 1 next self)
	.?! walk 1 next self)


onRow [] = stay
onRow (x:xs) = do
	(Cond Friend Ahead
		.?? (Skip .*. Skip .* onRow xs)
		.?! (walkForce .* x))

changeDirectionBy1 = 
	Flip 2 
	.??. Turn Left
	.?!. Turn Right

walkForce next = do
	action <- lift next
	Cond Rock Ahead 
		.?? changeDirectionBy1 action
		.?! forceAction (walk 1) action

forceAction action onSuccess = withSelf(\self ->
	(lifter2 action) onSuccess self)

protectRow3 = do
	onBottomColumn <- repeat 3 (Turn Left) .*.  walkForce .*. Turn Right .* walkForce stay
	onTopColumn    <- repeat 3 (Turn Left) .*.  walkForce .*. Turn Left  .* walkForce stay
	isBotColumn 2 onBottomColumn
		.*. isTopColumn 2 onTopColumn
		.*. repeat 3 (Turn Left) .*  walkForce stay

protectRow3rev = do
	onBottomColumn <- walkForce .*. Turn Left  .*. walkForce .* stay
	onTopColumn    <- walkForce .*. Turn Right .*. walkForce .* stay
	isBotColumn 2 searchForFood
		.*. isTopColumn 2 explore
		.*. isBotColumn 2 explore
		.*. isTopColumn 2 searchForFood
		.*. isBotColumn 2 searchForFood
		.*. isTopColumn 2 searchForFood
		.*. isBotColumn 2 onBottomColumn
		.*. isTopColumn 2 onTopColumn
		.*. isBotColumn 2 stay
		.*. isTopColumn 2 stay
		.*. turn180
		.*. walkForce
		.* stay

isBotColumn n onSuccess onFailure =
	repeat n (Turn Right) 
	.* (((Cond Friend Ahead))
		.?? (repeat n (Turn Left) .* onFailure)
		.?! (repeat n (Turn Left) .* onSuccess))

isTopColumn n onSuccess onFailure =
	repeat n (Turn Left) 
	.* (((Cond Friend Ahead))
		.?? (repeat n (Turn Right) .* onFailure)
		.?! (repeat n (Turn Right) .* onSuccess))

clearStartingArea = repeat 3 walkForce

initializeExplorers = do
	isBotColumn 2 explore
	.*. isTopColumn 2 explore
	.*  searchForFood

w8ForInit = repeat 120 Skip .* searchForFood

ants = snd $ run (onRow 
	[clearStartingArea .* searchForFood, 
	clearStartingArea .* searchForFood, 
	protectRow3rev, 
	w8ForInit,
	w8ForInit,
	w8ForInit,
	w8ForInit,
	w8ForInit,
	protectRow3,
	repeat 3 (Turn Left) .*. clearStartingArea .* explore, 
	repeat 3 (Turn Left) .*. clearStartingArea .* explore])

main = printFile $ generateText (snd ants)
printFile xs = putStr (unlines xs)

